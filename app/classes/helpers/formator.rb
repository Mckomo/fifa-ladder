class Formator
  
  # convert array to str with format {Player 1}, {Player 2}, ...
  def self.player_list( list = [] )
    list.map { |v| v.capitalize }.join( ", " )
  end
  
  # convert array to str with format {score home}:{score away}
  def self.match_result( result = [] )
    result.first( 2 ).join( ":" )
  end
end