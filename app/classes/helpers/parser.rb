class Parser
  
  # parse match result 
  def self.match_result( result )
    return result.scan( /[[:digit:]]+/u ).map! { |s| s.to_i } || []
  end
  
  # parse player list
  def self.player_list( list )
    return list.scan( /[[:alnum:]]{3,}/u ).sort.map! { |p| p.downcase } || []
  end
end