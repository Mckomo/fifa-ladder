class Stats
  
  # create ranking of player/tems who played in matches with requested player count
  def self.players_ranking( match_player_count = 4 )
    
    # get all matches with requested number of players 
    matches = Match.where( :player_count => match_player_count )
    
    # create players ranking object
    players_ranking = {} 
    
    # if there no matches
    if matches.size < 1
      return players_ranking
    end
    
    # loop through matches to create stats
    matches.each { |m| 
      
      # if there are more then 1 player per side, treat them as one player
      # join players name from both sides to create string (player representation in ranking)
      side_home = m.players[:home].join(', ')
      side_away = m.players[:away].join(', ')
      
      # create list of players to be added to ranking
      players_to_add = []
      players_to_add << side_home unless players_ranking.key? side_home
      players_to_add << side_away unless players_ranking.key? side_away
      
      # add each new player to ranking
      players_to_add.each { |p| 
      
        #create new player data object
        player_data = {
            :points             => 0,
            :fatigue            => 0,
            :matches_played     => 0,
            :wins               => 0,
            :wins_home          => 0,
            :wins_away          => 0,
            :draws              => 0,
            :draws_home         => 0,
            :draws_away         => 0,
            :losses             => 0,
            :losses_home        => 0,
            :losses_away        => 0,
            :scored_goals       => 0,
            :scored_goals_home  => 0,
            :scored_goals_away  => 0,
            :lost_goals         => 0,
            :lost_goals_home    => 0,
            :lost_goals_away    => 0,
        }
        
        # add it to ranking using player name as index
        players_ranking[p] = player_data
      }

      # process wins, draws and losses 
      case m.won?
        when 'home'
          # add points
          players_ranking[side_home][:points] += 3
          
          # increase standard wins and looses
          players_ranking[side_home][:wins] += 1
          players_ranking[side_away][:losses] += 1

          # increase wins and looses depending on sides
          players_ranking[side_home][:wins_home] += 1
          players_ranking[side_away][:losses_away] += 1
        when 'away'
          # add points
          players_ranking[side_away][:points] += 3
          
          # increase standard wins and looses
          players_ranking[side_away][:wins] += 1
          players_ranking[side_home][:losses] += 1

          # increase wins and looses depending on sides
          players_ranking[side_away][:wins_home] += 1
          players_ranking[side_home][:losses_away] += 1

        when 'draw'
          # add points
          players_ranking[side_home][:points] += 1
          players_ranking[side_away][:points] += 1
          
          # increase standard wins and looses
          players_ranking[side_away][:draws] += 1
          players_ranking[side_home][:draws] += 1

          players_ranking[side_away][:draws_home] += 1
          players_ranking[side_home][:draws_away] += 1
      end

      # count scored goals
      players_ranking[side_home][:scored_goals] += m.score[:home]
      players_ranking[side_away][:scored_goals] += m.score[:away]

      # count scored goals based on side
      players_ranking[side_home][:scored_goals_home] += m.score[:home]
      players_ranking[side_home][:scored_goals_away] += m.score[:away]

      # count lost goals
      players_ranking[side_home][:lost_goals] += m.score[:away]
      players_ranking[side_away][:lost_goals] += m.score[:home]

      # count lost goals based on side
      players_ranking[side_home][:lost_goals_home] += m.score[:away]
      players_ranking[side_away][:lost_goals_away] += m.score[:home]
    }
    
    # count agregate data
    players_ranking.each { |n, p|
      p[:matches_played] = p[:wins] + p[:draws] + p[:losses]
      p[:fatigue] = p[:points] > 0 ? ( p[:points].to_f / ( ( p[:wins] + p[:draws] + p[:losses] ) * 3.0 ) ).round(2) : 0
      players_ranking[n] = p
    } 
    
    # sort by fatigue and points
    players_ranking = players_ranking.sort_by { |k, v| v[:fatigue] }.sort_by { |k, v| v[:points] }.reverse
    
    return players_ranking
    
  end
  
  def self.player ( player )
    
    
    # get players home and away matches 
    m_home = player.matches.where( "appearances.side = 'home'" )
    m_away = player.matches.where( "appearances.side = 'away'" )
    
    # return nil if no matches were played
    if m_home.size + m_away.size < 1
      return nil
    end
    
    stats = {
      :points             => 0,
      :fatigue            => 0,
      :matches_played     => 0,
      :wins               => 0,
      :wins_home          => 0,
      :wins_away          => 0,
      :draws              => 0,
      :draws_home         => 0,
      :draws_away         => 0,
      :losses             => 0,
      :losses_home        => 0,
      :losses_away        => 0,
      :scored_goals       => 0,
      :scored_goals_home  => 0,
      :scored_goals_away  => 0,
      :lost_goals         => 0,
      :lost_goals_home    => 0,
      :lost_goals_away    => 0,
      :teams_played       => {},
      :favorite_team      => nil,
      :highest_win        => nil,
      :highest_win_match  => nil,
      :highest_loss       => nil,
      :highest_loss_match => nil,
    }
    
    player_stats = {
      :solo     => stats.clone,
      :in_team  => stats.clone,
      :all      => stats.clone,
    }
    
    # create interation probe to inspect matches and geather data about player
    iteration_probe = lambda { |data, player_side, m|
      
      # init value for player result in match
      player_result = nil      
      # set symbol of match - :solo or :in_team
      match_symbol = ( m.player_count > 2 ) ? :in_team : :solo
      
      # increment played match count
      data[match_symbol][:matches_played] += 1
      
      
      # get stats depending on side
      if player_side == 'home'
        
        # add scored goals
        data[match_symbol][:scored_goals] += m.score[:home]
        data[match_symbol][:scored_goals_home] += m.score[:home]
        
        # add lost goals
        data[match_symbol][:lost_goals] += m.score[:away]
        data[match_symbol][:lost_goals_home] += m.score[:away]
        
        # increment played team count
        if data[match_symbol][:teams_played].has_key?( m.team_home.id )
          data[match_symbol][:teams_played][m.team_home.id] += 1
        else
          data[match_symbol][:teams_played][m.team_home.id] = 1
        end
        
      else
        
        # add scored goal
        data[match_symbol][:scored_goals] += m.score[:away]
        data[match_symbol][:scored_goals_away] += m.score[:away]
        
        # add lost goals
        data[match_symbol][:lost_goals] += m.score[:home]
        data[match_symbol][:lost_goals_home] += m.score[:home]
        
        # increment played team count
        if data[match_symbol][:teams_played].has_key?( m.team_away.id )
          data[match_symbol][:teams_played][m.team_away.id] += 1
        else
          data[match_symbol][:teams_played][m.team_away.id] = 1
        end
        
      end
      
      # get stats depending on side and result        
      case m.won?
        when 'home'
          # player won as home side
          if player_side == 'home'
            
            # set player result
            player_result = 'win'

            # increment wins home
            data[match_symbol][:wins_home] += 1
 
          # player lost as home side
          else
            # set player result
            player_result = 'loss'
          
            # increment losses home
            data[match_symbol][:losses_home] += 1
            
          end
        when 'away'
          # player won as away side
          if player_side == 'away'
              
            # set player result
            player_result = 'win'
          
            # increment wins away
            data[match_symbol][:wins_away] += 1
            
          # player lost as away side   
          else
            
            # set player result
            player_result = 'loss'
          
            # increment losses away
            data[match_symbol][:losses_away] += 1
            
          end
        when 'draw'
          
          # set player result
          player_result = 'draw'
          
          # player draw as home side
          if player_side == 'home'
          
            # increment draws home
            data[match_symbol][:draws_home] += 1
            
          # player draw as away side
          else
            
            # increment draws away
            data[match_symbol][:draws_away] += 1
            
          end
      end
      
      # get stats depending on result
      case player_result
        when 'win'
          
            # increment wins
            data[match_symbol][:wins] += 1
            # add points
            data[match_symbol][:points] += 3
              
            # swap highest win if match was won with higher adventage
            if ! data[match_symbol][:highest_win] or m.score.values.max - m.score.values.min > data[match_symbol][:highest_win].max - data[match_symbol][:highest_win].min
              data[match_symbol][:highest_win] = m.score.values
              data[match_symbol][:highest_win_match] = m 
            end
              
        when 'draw'
          
            # increment draws
            data[match_symbol][:draws] += 1
            # add points
            data[match_symbol][:points] += 1
            
        when 'loss'
        
          # increment losses
          data[match_symbol][:losses] += 1
                      
          # swap highest loss if match was lost with lowest adventage
          if ! data[match_symbol][:highest_loss] or m.score.values.max - m.score.values.min > data[match_symbol][:highest_loss].max - data[match_symbol][:highest_loss].min
            data[match_symbol][:highest_loss] = m.score.values
            data[match_symbol][:highest_loss_match] = m
          end
          
      end
      
    }
    
    # run probe on home maches
    m_home.each { |m|
      iteration_probe.call( player_stats, 'home', m )      
    }
    
    # run probe on away maches
    m_away.each { |m|
      iteration_probe.call( player_stats, 'away', m )
    }
    
    # join solo and in team stas to get stats from all matches
    player_stats[:all] = player_stats[:solo].merge( player_stats[:in_team] ) { |key, old_val, new_val| 
      
      # if both values are nil return nil
      if ! ( old_val or new_val ) 
        next nil
      end
      
      # if only one of values is nil return it
      if old_val.class == NilClass or new_val.class == NilClass
        next old_val || new_val
      end
      
      # return nil for Match model holder ( to decide latter )
      if key.to_s.include?( "_match" )
        next nil
      end
      
      # if value is hash, also merge them
      if old_val.class == Hash
        next old_val.merge( new_val ) { |i, v1, v2| v1 + v2 }
      end
      
      # if value is hash, also merge them
      if old_val.class == Hash
        next old_val.merge( new_val ) { |i, v1, v2| v1 + v2 }
      end
      
      # else, just add values
      new_val + old_val 
    }
    
    # if there are two highest wins in :all stats, choose higher
    if player_stats[:all][:highest_win] and player_stats[:all][:highest_win].size == 4
        
        # get score of highest win in both modes
        in_team_score = player_stats[:in_team][:highest_win].sort
        solo_score = player_stats[:solo][:highest_win].sort
        
        # choose score of the mode with the biggest goals adventage ( prefere solo mode )
        if solo_score[1] - solo_score[0] >= in_team_score[1] - in_team_score[0]
          player_stats[:all][:highest_win] = player_stats[:solo][:highest_win]
          player_stats[:all][:highest_win_match] = player_stats[:solo][:highest_win_match]
        else
          player_stats[:all][:highest_win] = player_stats[:in_team][:highest_win]
          player_stats[:all][:highest_win_match] = player_stats[:in_team][:highest_win_match]
        end
        
    end
    
    # if there are two highest losses in :all stats, choose higher
    if player_stats[:all][:highest_loss] and player_stats[:all][:highest_loss].size == 4
      
      # get score of highest loss in both modes
      in_team_score = player_stats[:in_team][:highest_loss].sort
      solo_score = player_stats[:solo][:highest_loss].sort
      
      # choose score of the mode with the biggest goals adventage ( prefere solo mode )
      if solo_score[1] - solo_score[0] >= in_team_score[1] - in_team_score[0]
        player_stats[:all][:highest_loss] = player_stats[:solo][:highest_loss]
        player_stats[:all][:highest_loss_match] = player_stats[:solo][:highest_loss_match]
      else
        player_stats[:all][:highest_loss] = player_stats[:in_team][:highest_loss]
        player_stats[:all][:highest_loss_match] = player_stats[:in_team][:highest_loss_match]
      end
      
  end
    
    # proces agregate data for each type of stats
    player_stats.keys.each { |k| 
      
      # if any matechs played
      if player_stats[k][:matches_played] > 0
        
        # sort played teams count
        player_stats[k][:teams_played] = player_stats[k][:teams_played].sort_by { |k, v| v }.reverse
        
        # set favorite team
        favorite_team_id = player_stats[k][:teams_played].first[0]      
        player_stats[k][:favorite_team] = FootballTeam.find( favorite_team_id )
        
        # set fatigue
        player_stats[k][:fatigue] = (Float( player_stats[k][:points] ) / ( Float( player_stats[k][:matches_played] ) * 3 ) ).round(2)
        
      end
      
    }
    
    return player_stats
    
  end
    
end
