# encoding: utf-8

class ControllerRanking < Controller
  
  # Include terminal-table gem namespace
  include Terminal
  
  @config = {
    'wait_after?' => true
  }
  
  def run
    choice = Interface.menu( {
      1 => "Wyświetl ranking 1 vs 1",
      2 => "Wyświetl ranking 2 vs 2",
      0 => "Wyjdź",
    } )
    
    case choice
      when '1'
        # ranking 1v1 games
        ranking( 2 )
      when '2'
        # ranking  
        ranking( 4 )
      when '0'
        return
    end
        
  end
  
  # print ranking of players 
  def ranking( player_count )

    if ! ( player_count % 2 )
      raise "No ranking for matches with odd players number"
    end
    
    # prepare console for ranking
    Console.flush
    Console.output( "Ranking rozgrywek #{player_count / 2} vs #{player_count / 2}", 1 )
    
    # init table for rankinh
    table = Table.new
    table.headings = [
      ( player_count > 2 ) ? 'Drużyna' : 'Zwodnik' ,
      'Rozegrane mecze',
      'Wygrane',
      'Remisy',
      'Przegrane',
      'Zdobyte bramki',
      'Stracone bramki',
      'Forma',
      'Punkty', 
    ]
    
    # get players ranking
    players_ranking = Stats.players_ranking( player_count )
    
    # put ranking data into table
    players_ranking.each { |n, p| 
      table.add_row [ 
        n, 
        p[:matches_played],
        p[:wins],
        p[:draws],
        p[:losses],
        p[:scored_goals],
        p[:lost_goals],
        p[:fatigue],
        p[:points]
      ]
        
      if table.rows.size < players_ranking.size
        table.add_separator
      end
    }
    
    Console.output "#{table}"
    
  end  
    
end