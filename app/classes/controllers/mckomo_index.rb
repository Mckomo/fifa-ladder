# encoding: utf-8

class ControllerMckomoIndex < Controller
  
  @config = {
    'wait_after?' => true
  }
  
  def run
        
    # Create stats print for each players
    Player.all.each { |p| 
      
      # fetch stats
      stats  = Stats.player( p )
      
      if ! stats
        return
      end
      
      Console.output( "Nick gracza: #{p.name}", 1 )
      # start stats print for all matches
      Console.output( "Statystyki dla wszystkich rozgrywek." )
      Console.output( "Zdobyte punkty: #{stats[:all][:points]}" ) 
      Console.output( "Aktualna forma: #{stats[:all][:fatigue]}" )
      Console.output( "Rozegrane mecze: #{stats[:all][:matches_played]}" )
      Console.output( "Wygrane: #{stats[:all][:wins]}" )
      Console.output( "Remisy: #{stats[:all][:draws]}" )
      Console.output( "Przegrane: #{stats[:all][:losses]}", 1 )
    
      Console.output( "Zdobyte bramki: #{stats[:all][:scored_goals]}" )
      Console.output( "Stracone bramki: #{stats[:all][:lost_goals]}", 1 )
     
      Console.output( "Ulubiona drużyna: #{stats[:all][:favorite_team].name} ( #{stats[:all][:teams_played].first[1]} )" )
      if stats[:all][:highest_win]
        Console.output( "Najwyższa wygrana: #{stats[:all][:highest_win_match].score( true )} w meczu \"#{stats[:all][:highest_win_match].to_s}\"" )
      end
      if stats[:all][:highest_loss] 
        Console.output( "Najwyższa przegrana: #{stats[:all][:highest_loss_match].score( true )} w meczu \"#{stats[:all][:highest_loss_match].to_s}\"")
      end
      
      Console.br
      
      # start stats print for in team matches
      Console.output( "Statystyki dla rozgrywek w drużynie." )
      Console.output( "Zdobyte punkty: #{stats[:in_team][:points]}" ) 
      Console.output( "Aktualna forma: #{stats[:in_team][:fatigue]}" )
      Console.output( "Rozegrane mecze: #{stats[:in_team][:matches_played]}" )
      Console.output( "Wygrane: #{stats[:in_team][:wins]}" )
      Console.output( "Remisy: #{stats[:in_team][:draws]}" )
      Console.output( "Przegrane: #{stats[:in_team][:losses]}", 1 )
    
      Console.output( "Zdobyte bramki: #{stats[:in_team][:scored_goals]}" )
      Console.output( "Stracone bramki: #{stats[:in_team][:lost_goals]}", 1 )
      
      if stats[:in_team][:favorite_team]
        Console.output( "Ulubiona drużyna: #{stats[:in_team][:favorite_team].name} ( #{stats[:in_team][:teams_played].first[1]} )" )
      end
      if stats[:in_team][:highest_win]
        Console.output( "Najwyższa wygrana: #{stats[:in_team][:highest_win_match].score( true )} w meczu \"#{stats[:in_team][:highest_win_match].to_s}\"" )
      end
      if stats[:in_team][:highest_loss] 
        Console.output( "Najwyższa przegrana: #{stats[:in_team][:highest_loss_match].score( true )} w meczu \"#{stats[:in_team][:highest_loss_match].to_s}\"")
      end
      
      Console.br
      
      # start stats print for solo matches
      Console.output( "Statystyki dla rozgrywek indywidualnych." )
      Console.output( "Zdobyte punkty: #{stats[:solo][:points]}" ) 
      Console.output( "Aktualna forma: #{stats[:solo][:fatigue]}" )
      Console.output( "Rozegrane mecze: #{stats[:solo][:matches_played]}" )
      Console.output( "Wygrane: #{stats[:solo][:wins]}" )
      Console.output( "Remisy: #{stats[:solo][:draws]}" )
      Console.output( "Przegrane: #{stats[:solo][:losses]}", 1 )
    
      Console.output( "Zdobyte bramki: #{stats[:solo][:scored_goals]}" )
      Console.output( "Stracone bramki: #{stats[:solo][:lost_goals]}", 1 )
       
      if stats[:solo][:favorite_team] 
        Console.output( "Ulubiona drużyna: #{stats[:solo][:favorite_team].name} ( #{stats[:solo][:teams_played].first[1]} )" )
      end
      if stats[:solo][:highest_win]
        Console.output( "Najwyższa wygrana: #{stats[:solo][:highest_win_match].score( true )} w meczu \"#{stats[:solo][:highest_win_match].to_s}\"" )
      end
      if stats[:solo][:highest_loss] 
        Console.output( "Najwyższa przegrana: #{stats[:solo][:highest_loss_match].score( true )} w meczu \"#{stats[:solo][:highest_loss_match].to_s}\"")
      end
      
      # print spacer
      Console.output( "-" * 50, 1) 
        
    }
    
  end
end