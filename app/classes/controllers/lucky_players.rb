# encoding: utf-8

class ControllerLuckyPlayers < Controller
  
  @config = {
    'wait_after?' => true
  }
  
  def run
    
    # list of two lucky players
    lucky_pair = []
    
    # get list of all players
    players = Interface.list({
      :label              => "Podaj nicki kolejnych zawodników:",
      :alert_under_limit  => "Musisz podać przynajmniej 2 zawodników.",
    })
    
    # chose 2 random players from list
    lucky_players = players.sample( 2 )
    
    Console.output( "Wylosowanie zawodnicy to:", 1 )
    # wait for it!
    sleep(0.5)
    Console.output( "Wait for it!", 1 )
    sleep(2)
    Console.output( lucky_players.join(" i ") )
    
  end
  
end