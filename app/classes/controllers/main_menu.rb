# encoding: utf-8

class ControllerMainMenu < Controller
  
  def run
    
    choice = Interface.menu( {
      1 => 'Wylosuj 2 zawodników',
      2 => 'Rozegraj mecz',
      3 => 'Wyświetl archiwum rozgrywek',
      4 => 'Wyświetl ranking graczy',
      5 => 'Wyśtwetl Mckomo Player Index',
      0 => 'Zakończ pracę programu',
      } )
    
    case choice
      when '1'
        CallStack.push( 'lucky_players' )
      when '2'
        CallStack.push( 'match' )
      when '3'
        CallStack.push( 'archive' )
      when '4'
        CallStack.push( 'ranking' )
      when '5'
        CallStack.push( 'mckomo_index' )
      when '0'
        Application.kill
      end
  end

end
