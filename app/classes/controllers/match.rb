# encoding: utf-8

class ControllerMatch < Controller
  
  @config = {
    'wait_after?' => true
  }
  
  def run
    
    # create new matach obj 
    match    = Match.new
    match.id = Match.count + 1
    
    # create form to geather match data
    data = Interface.form( {
      :players_home => { 
        :label      => "Podaj nick(i) gospodarz(a/y):",
        :filter     => lambda { |v| Parser.player_list( v ) },
        :validation => lambda { |v| v.size >= 1 },
        :format     => lambda { |v| Formator.player_list( v ) },
      },
      :players_away => {
        :label      => "Podaj nick(i) gości(a):",
        :filter     => lambda { |v| Parser.player_list( v ) },
        :validation => lambda { |v| v.size >= 1 },
        :format     => lambda { |v| Formator.player_list( v ) },
      }, 
      :team_home    => {
        :label      => "Podaj drużynę gospodarzy:",
        :filter     => lambda { |v| Finder.most_similar( v, FootballTeam.all.map { |t| t.name } ) },
        :validation => lambda { |v| v.length > 2 },
        :format     => lambda { |v| v },
      },
      :team_away    => {
        :label      => "Podaj drużynę gości:",
        :filter     => lambda { |v| Finder.most_similar( v, FootballTeam.all.map { |t| t.name } ) },
        :validation => lambda { |v| v.length > 2 },
        :format     => lambda { |v| v },
      },
      :result       => {
        :label      => "Podaj wynik spotkania:",
        :filter     => lambda { |v| Parser.match_result( v ) },
        :validation => lambda { |v| v.size == 2 },
        :format     => lambda { |v| Formator.match_result( v ) },
      } 
    }, {
      :print_format => "Rozegrany mecz: #{:players_home} (#{:team_home}) #{:result} (#{:team_away}) #{:players_away}",
    } )
    
    # finish if no data returned 
    return unless data
    
    # save match score
    match.score_home  = data[:result][0]
    match.score_away  = data[:result][1]
    
    # save ids of football teams
    match.team_home   = FootballTeam.find_by_name( data[:team_home] )
    match.team_away   = FootballTeam.find_by_name( data[:team_away] )
     
    # Create associations between players and match
    data[:players_home].each { |p| player =  Player.find_or_create_by_name( p ); player.appearances.create( :match_id => match.id, :side => 'home') }
    data[:players_away].each { |p| player =  Player.find_or_create_by_name( p ); player.appearances.create( :match_id => match.id, :side => 'away') }
    
    if match.save
      Console.output "Mecz zapisany!"
    else
      raise "Database error! Can't save match."
    end  
      
  end
end
