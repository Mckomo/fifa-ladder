# encoding: utf-8

class ControllerArchive < Controller
  
  # Include terminal-table gem namespace
  include Terminal
  
  @config = {
    'wait_after?' => true,
  }
  
  def run
    
    # get all matches
    matches = Match.all
    rows = []
    
    # create table with matches hisotry
    table = Table.new
    table.headings  = [ 'Gospodarze', 'Goście', 'Wynik', 'Drużyna gospodarzy', 'Drużyna gości', 'Data spotkania' ] 
    table.rows = []
   
    # prepare table data
    if matches.size > 0
      i = 1
      matches.each { |m|
        p_home = []
        p_away = []
        table.add_row ( [ 
          m.players[:home].join(', '),
          m.players[:away].join(', '), 
          m.score( true ), 
          m.team[:home], 
          m.team[:away], 
          m.created_at 
        ] )
        table.add_separator unless ( matches.size == i )
        i += 1
      }  
    else
      table.title = "Brak rozegranych meczy. Do dzieła!"
    end
    
    # append data    
    
    # Output table 
    Console.output "Terminarz rozgrywek"
    Console.br
    Console.output "#{table}"   
  end
end