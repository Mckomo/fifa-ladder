class Player < ActiveRecord::Base 
  
  has_many :appearances
  has_many :matches, :through => :appearances
  
  def name
    read_attribute( :name ).capitalize
  end
  
  def name=(val)
    write_attribute( :name, val.downcase )
  end
end