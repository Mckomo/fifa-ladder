class FootballTeam < ActiveRecord::Base
  has_many :matches, :as => :team_home
  has_many :matches, :as => :team_away
end