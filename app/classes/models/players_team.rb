class PlayersTeam
  
  attr_accessor :players, :matches
  
  def initialize( player_list = [] )
    
    @players = []
    @matches = []
    
    # get each player from list
    player_list.each { |id| 
      player = Player.find( id )
      # add player to team if not nil
      @players << player unless !player
    }
    
    puts @players
        
    # return nil if team has less then 2 members
    if @players.size < 2
      return
    end
    
    puts 'test'
    
    
    # prepare sql statement to find teams matches
    sql = 
      "SELECT matches.* FROM 
      matches INNER JOIN
      	(SELECT appearances.*, COUNT(match_id) AS player_count FROM appearances
      	WHERE appearances.player_id IN ( IDS ) AND side = 'home'
      	GROUP BY match_id
      	HAVING player_count == ? ) AS appearances
      ON matches.id = appearances.match_id"
      
    # create string like "?, ?, ?, ... ?"
    question_mars = Array.new( player_list.size ).fill( "?" ).join(", ")
    sql.sub!( "IDS", question_mars )
    
    puts "#{sql}"
    
    # get teams matches
    @matches = Match.find_by_sql( [ sql ] + player_list + [ player_list.size ] )
    
  end
  
  
end 
  
  