class Appearance < ActiveRecord::Base 
    belongs_to :match
    belongs_to :players_home, :foreign_key => "player_id"
    belongs_to :players_away, :foreign_key => "player_id"
end