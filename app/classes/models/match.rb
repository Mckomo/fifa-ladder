class Match < ActiveRecord::Base 
  
  has_many :appearances
  has_many :players_home, :through => :appearances, :class_name => "Player", :conditions => "side = 'home'"
  has_many :players_away, :through => :appearances, :class_name => "Player", :conditions => "side = 'away'"
  
  belongs_to :team_home, :class_name => "FootballTeam", :foreign_key => "team_home"
  belongs_to :team_away, :class_name => "FootballTeam", :foreign_key => "team_away"
  
  after_create :count_players
  
  # return match result in either string or array
  def score( to_s = false )
    if to_s
      "#{score_home}:#{score_away}"
    else
      { 
       :home => score_home,
       :away => score_away
      }
   end
  end
    
  # return hash with names of teams that played in match
  def team
    {
      :home => team_home.name, 
      :away => team_away.name 
    }
  end
  
  # return win side
  def won?
    if score[:home] > score[:away]
      return 'home'
    elsif score[:home] < score[:away]
      return 'away'
    else
      return 'draw'
    end
  end
  
  # return array with names of players from each side
  def players
    players = {
      :home => [],
      :away => [],
    }
    players_home.each { |p| players[:home].push( p.name ) }
    players_away.each { |p| players[:away].push( p.name ) }
    
    players
  end
  
  # string representation of match
  def to_s
    "#{self.players[:home].join(", ")} (#{self.team[:home]}) #{self.score( true )} (#{self.team[:away]}) #{self.players[:away].join(", ")}"
  end
    
  private
  
  def count_players
        
    # get players
    p_home = self.players_home || []
    p_away = self.players_away || []
    
    # make sure that teams are even
    if players_home.size != players_away.size
      raise "Trying to save match with uneven teams (#{p_home.size} to #{p_away.size})."
    end
    
    # save player count
    self.player_count = p_home.size + p_away.size
    self.save
     
  end
  
  # protect data from changes
  def after_find
    self.freeze      
  end 
  
end