class CreateAppearances < ActiveRecord::Migration
  
  def up
    create_table :appearances do |t|
      t.string  :player_id
      t.integer :match_id
      t.string  :side
    end
  end
  
  def down
    drop_table :appearances
  end
  
end