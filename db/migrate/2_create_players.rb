class CreatePlayers < ActiveRecord::Migration
  
  def up
    create_table :players do |t|
      t.string :name
      
      t.timestamps
    end
    add_index :players, :name, :unique => true
  end
  
  def down
    drop_table :players
  end
  
end