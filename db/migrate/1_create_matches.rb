class CreateMatches < ActiveRecord::Migration

  def up
    create_table :matches do |t|
      t.integer :player_count
      t.integer :team_home, :default => 0
      t.integer :team_away, :default => 0
      t.integer :score_home
      t.integer :score_away
      t.timestamps
    end
  end
  
  def down
    drop_table :matches
  end
  
end