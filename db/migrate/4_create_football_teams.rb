#encoding: utf-8

class CreateFootballTeams < ActiveRecord::Migration
  
  @@team_list = [
    #Spain
    'Athletic Bilbao',
    'Atletico Madryt',
    'Celta Vigo',
    'Deportivo La Coruna',
    'Espanyol Barcelona',
    'FC Barcelona',
    'Getafe CF',
    'Granada CF',
    'Levante UD',
    'Malaga CF',
    'Osasuna Pampeluna',
    'Rayo Vallecano',
    'Real Betis Sewilla',
    'Real Madryt',
    'Real Mallorca',
    'Real Saragossa',
    'Real Sociedad',
    'Real Valladolid',
    'Sevilla FC',
    'Valencia CF',
    #England
    'Arsenal Londyn',
    'Aston Villa',
    'Chelsea Londyn',
    'Everton',
    'Fulham',
    'Liverpool',
    'Manchester City',
    'Manchester United',
    'Newcastle United',
    'Norwich City',
    'Queens Park Rangers',
    'Reading',
    'Southampton',
    'Stoke City',
    'Sunderland',
    'Swansea City',
    'Tottenham Hotspur',
    'West Bromwich Albion',
    'West Ham United',
    'Wigan Athletic',
    # Germany
    'Bayer Leverkusen',
    'Bayern Monachium',
    'Borussia Dortmund',
    'Borussia Mönchengladbach',
    'Eintracht Frankfurt',
    'FC Augsburg',
    'FC Nürnberg',
    'Fortuna Düsseldorf',
    'FSV Mainz 05',
    'Greuther Fürth',
    'Hamburger SV',
    'Hannover 96',
    'SC Freiburg',
    'Schalke 04 Gelsenkirchen',
    'TSG 1899 Hoffenheim',
    'VfB Stuttgart',
    'VfL Wolfsburg',
    'Werder Brema',
    # Italy
    'AC Milan',
    'AC Siena',
    'ACF Fiorentina',
    'AS Roma',
    'Atalanta Bergamo',
    'Bologna FC',
    'Cagliari Calcio',
    'Catania Calcio',
    'Chievo Verona',
    'FC Genoa 1893',
    'FC Parma',
    'FC Torino',
    'Inter Mediolan',
    'Juventus Tury',
    'Napoli',
    'Pescara Calcio',
    'Sampdoria Genua',
    'SS Lazio Rzym',
    'Udinese Calcio',
    'US Palermo',
    # France
    'AC Ajaccio',
    'AS Nancy',
    'AS Saint-Etienne',
    'Evian Thonon Gaillard',
    'FC Lorient',
    'FC Sochaux',
    'Girondins Bordeaux',
    'Lille OSC',
    'Montpellier',
    'OGC Nice',
    'Olympique Lyon',
    'Olympique Marsylia',
    'Paris Saint Germain',
    'SC Bastia',
    'Stade Brestois',
    'Stade de Reims',
    'Stade Rennais',
    'Toulouse FC',
    'Troyes',
    'Valenciennes',
    # Scotland
    'Celtic Glasgow',
    # Brasil
    'Atlético Mineiro',
  ]
  
  def up
    create_table :football_teams do |t|
      t.string  :name
    end
    
    # append teams from list to db  
    @@team_list.each do |t|
      execute "INSERT INTO football_teams (name) VALUES ('#{t}')"
    end
    
  end
  
  def down
    drop_table :football_teams
  end
  
end