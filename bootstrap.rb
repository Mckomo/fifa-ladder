# define app constants
ROOT        = Dir.pwd + '/'
CORE_PATH   = ROOT + 'core/'
APP_PATH    = ROOT + 'app/'

DB_PATH     = ROOT + 'db/' 
CONFIG_PATH = ROOT + 'config/' 

CLASS_PATH  = APP_PATH + 'classes/'
VIEW_PATH   = APP_PATH + 'views/'

# include required gems
require 'rubygems'
require 'bundler'

Bundler.setup( :default )

require 'erb'
require 'sqlite3'
require 'active_record'
require 'highline/import'
require 'yaml'
require 'text'
require 'terminal-table'

# include core classes
Dir[ CORE_PATH + '*.rb' ].each { |f| require f }  

# include app classes
Dir[ CLASS_PATH + '*/*.rb' ].each { |f| require f }  
