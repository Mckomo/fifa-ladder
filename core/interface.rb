## encoding: utf-8

class Interface
    
    
    # poll default params
    @@poll_defaults = {
      :label                  => "Podaj wartość:",
      :alert_incorrect_input  => "Wprowadziłeś złe dane. Spróbuj jeszcze raz.",
    }
    
    # list default params
    @@list_defaults = {
        :label              => "Podaj kolejne elementy:",
        :alert_under_limit  => "Podałeś za małą liczbę elementów.",
        :minimum_length     => 2,
        :maximum_length     => 0, # 0 == no limit
    }
  
    # list default params
    @@menu_defaults = {
        :message_prompt     => "Co chesz zrobić ?",
    }
    
    
    def self.form( polls, params = {} )
      
      # init container for form data
      data = {}
      # prepare text representation of form
      print_format = params[:print_format]
      
      maciej = "maciej"
            
      # print form till user accept it
      begin        
        # prepare console for form
        Console.flush()
        
        # loop each poll
        polls.each { |k, p|
          # get data from pool
          answer = self.poll( p )
                    
          # if answer is string test if it is not interrupt message
          if answer.class == String 
                        
            # if input equals "q" or "x" quit processing form and return nil
            return nil unless answer.scan( /^[qx]$/i ).size == 0 
          
            # if input equals "r" restart processing form
            return self.form( polls, params ) unless answer.scan( /^r$/i ).size == 0
          end
                
          data[k] = answer
          # replace print format symbols with data from form
          print_format.gsub!( "#{k}", p[:format].call( answer ) ) unless ( ! print_format or p[:format].class != Proc )    
        }
        
        # print geathred data and ask user if given answers are correct    
        Console.br
        Console.output( print_format, 1 )
        form_incorrect = Console.input( "Czy wypełnione dane są poprawne (t/n)?" ) == 't' ? false : true; 
        
      end while form_incorrect 
      
      return data
      
    end
    
    def self.poll( params = {} )
      
      # prepare poll components  
      filter = params[:filter]
      validation = params[:validation]
      
      # dialog list
      label = params.has_key?( :label ) ? params[:label] : @@poll_defaults[:label]
      alert_incorrect_input = params.has_key?( :alert_incorrect_input ) ? params[:alert_incorrect_input] : @@poll_defaults[:alert_incorrect_input]
      
      # ask for input till answer is correct
      begin
        
        # get input from user 
        answer = Console.input( label ).to_s
                
        # check if input equals quit ( "q" ) or restart ( "r" ) message
        # if so retrun it 
       return answer unless answer.scan( /^[rqx]$/i ).size == 0
 
        # assume that input is correct
        answer_correct = true
        # apply filter if given
        if filter.class == Proc
          answer = filter.call( answer )
        end
        # validate ansewer if test given
        if validation.class == Proc 
          if ! validation.call( answer )
            answer_correct = false
            Console.output( alert_incorrect_input )
          end
        end
      end while ! answer_correct
      
      return answer
      
    end
    
    # get user choice of menu options
    def self.menu( options = {}, params = {} )
        
        # set params of menu
        message_prompt = params.has_key?( :message_prompt ) ? params[:message_prompt] : @@menu_defaults[:message_prompt]
        
        # prepare console for menu
        Console.flush
        
        # print menu header if given
        Console.output( params[:header], 1 ) unless ! params[:header]
                
        # output each option
        options.each do |k, option|
            Console.output( "#{k}. #{option}" )
        end
        
        # ask for input
        choice = Console.input( message_prompt, 1 )

        return choice
    end
  
    # get list of user input
    def self.list( params = {} )
        
        # set list min length, defualt 2 elements
        min_length = params.has_key?( :minimum_length ) ? params[:minimum_length] : @@list_defaults[:minimum_length]
        # set list max length, defualt no limit ( 0 )
        max_length = params.has_key?( :maximum_length ) ? params[:maximum_length] : @@list_defaults[:maximum_length]
        # set instruction message
        label = params.has_key?( :label ) ? params[:label] : @@list_defaults[:label]
        # set underlimit message
        msg_under_limit = params.has_key?( :alert_under_limit ) ? params[:alert_under_limit] : @@list_defaults[:alert_under_limit]
        
        begin
            # list of user input
            list = []
            list_finished = false
            # clear console
            Console.flush 
            # output instruction message 
            Console.output( label, 1 )
            # add element to list till input is empty or list reached maximum limit
            begin 
              # get user input
              input = Console.input( "#{list.size + 1}." )
              # list is complite if input is empty or list reached maximum limit
              if ( input.empty? or ( max_length != 0 and list.length + 1 >= max_length ) )
                list_finished = true
              else
                list.push(input)
              end                
            end while ! list_finished
            # count list and check if it equal or greater then minimum limit
            if list.length < min_length
                # inform user about not metting the limit
                Console.output msg_under_limit
                # give user time to read notification  
                Application.pause
            end
        end while ( list.length < min_length ) 
        
        list
    end

    # show user message wrong choice
    def self.message( msg = "", params = {} )
        Console.output msg
        Application.pause
    end  
      
end