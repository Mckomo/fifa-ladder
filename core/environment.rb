class Environment
  
  @@online = false
  @@mode = 'production'
  
  def self.init
    
    # set runtime mode
    case ARGV[0]
      when 'development'
        @@mode = 'development'
      when 'test'
        @@mode = 'test'
    end
    
    # set if app has access to internet
    @@online = false 
  end
  
  def self.mode
    @@mode
  end
  
  def self.online 
    @@online
  end
  
end