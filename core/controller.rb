# encoding: utf-8

class Controller
    
    @@defualts = {
        'wait_after?' => false,
    }
    
    @config = {}    
  
    def initialize
        @config = @@defualts.merge( ( self.class.config.class == Hash ) ? self.class.config : {} )
    end 
    
    def self.config
        @config
    end
    
    def config( element = "" )
        element.empty? ? @config : @config[element]
    end

    # abstract method to be overridden 
    def run
        raise "Abstract method ! Run method must be overriden."
    end
    
end