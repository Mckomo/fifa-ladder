class Console
    
  # left offset of terminal window
  @@offset = "    "
  # param for last input 
  @@last_input = nil
    
  # clear console window
  def self.clear
    system("clear") 
  end
  
  # print string with offset
  def self.output( str, bottom_margin = 0 )
    # output each line of text
    str.each_line do |line|
      say @@offset + line
    end
    # print bottom margin
    self.margin( bottom_margin ) unless bottom_margin < 1 
  end
  
  # ask user for input
  def self.input( msg = "", top_margin = 0 )
    # print top margin
    self.margin( top_margin ) unless top_margin < 1 
    # make same space for input dialog
    if msg.length > 0
      @@last_input = ask @@offset + msg + " "
    else
      @@last_input = ask @@offset
    end
    return @@last_input
  end
  
  # return last_input
  def self.last_input
    @@last_input
  end
  
  # print bottom margin of text 
  def self.margin( lines )
    for i in 1..lines
      self.br
    end
  end
  
  # print new line
  def self.br
    say "\n"
  end 
  
  # clear console and render main view
  def self.flush
    self.clear
    View.render '_top' 
  end
  
end