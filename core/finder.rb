class Finder 
  
  # include text gem namespace
  include Text
  
  # use Simon White method to test similirity
  @@white = WhiteSimilarity.new
  
  def self.most_similar( scrap, collection )
    
    best_match = nil
    highest_similarity = 0
    
    # loop through collection to find the most similar element
    collection.each { |c|
      similarity = @@white.similarity( c, scrap )
      if similarity > highest_similarity
        highest_similarity = similarity
        best_match = c 
      end
    }
    
    # return best match or empty string 
    return best_match || ""
    
  end
end
