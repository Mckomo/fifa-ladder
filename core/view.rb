class View

  # container of views
  @@views_cache = {}

  def self.render( view_name )
    # check if view is stored
    if ( @@views_cache.has_key?( view_name ) )
      # get it from container
      view = @@views_cache[view_name]
    else
      # save file content as view
      view = ERB.new( File.read( VIEW_PATH + "#{view_name}.txt.erb" ) ).result
      # store view
      @@views_cache[view_name] = view
    end
    # output view
    Console.output view
  end
end