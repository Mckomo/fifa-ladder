class Inflector
  # camel case the string with separator  
  def self.camelcase_with_separator(str, spr=" ")
    # split string by separator
    word_arr = str.split(spr)
    # uppercase first letter in every world
    word_arr.map!{ |x| x = x.capitalize }
    # join array with given glue
    return word_arr.join()
  end
end