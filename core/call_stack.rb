class CallStack
    
    # stack container
    @@container = []
    
    # last poped element
    @@last_poped
    
    def self.push( controller, go_back_after = false )
        # push again last controller to make him back after to be pushed controller runtime
        if go_back_after
             @@container.push( self.last_poped )
         end
        @@container.push( controller )
    end
    
    def self.pop
      @@poped_element = @@container.pop
    end
    
    def self.last_poped 
      @@last_poped
    end
end