class Application
    
    @@initialized = false
    @@default_controller = "main_menu"
    @@continue_message = "-- KONTYNUUJ --\n"
    @@controllers_stack
    @@controllers_cache = {}
    @@keep_alive = false
  
    def self.start()
        # init app 
        self.init()
        # triger actions unless app is not alive
        while @@keep_alive do
            self.triger( CallStack.pop )
        end
        # than exit
        self.exit()
    end
  
    def self.pause()
        # ask user to continue
        Console.br
        Console.input @@continue_message
    end
  
    # kill app    
    def self.kill()    
        @@keep_alive = false
    end
  
    def self.exit()
        # show goodbye message for 2 secounds
        Console.output "Do zobaczenia !"
        sleep(2)
        # clear console messs
        Console.clear 
        # kill program process
        Process.exit
    end
  
    def self.triger( controller_name )
        
        to_triger = controller_name ? controller_name : @@default_controller;
        
        # load controller
        controller = self.load( to_triger )
        # flush console window before running controller
        Console.flush
        # run main method of controller
        controller.method("run").call
        # give user chance to read output 
        self.pause unless ! controller.config('wait_after?')
        # end of triger process  
    end
    
    private
  
    # initiation method
    def self.init()  
        # check if class was inited before
        if @@initialized 
            raise "Aplication has been already initialized."
        end
        
        # init app environment
        Environment.init
        
        # load db config
        db_config = YAML::load( File.open( CONFIG_PATH + 'db.yml' ), Environment.mode )

        # set up data base
        ActiveRecord::Base.establish_connection( db_config[Environment.mode] )
        
        # give life to app
        @@keep_alive = true
    end
        
    # load controllers
    def self.load( controller_file_name )
        # check if controller is stored
        if ( @@controllers_cache.has_key?(controller_file_name) )
            # get it from container
            controller = @@controllers_cache[controller_file_name]
        else
            # convert file name to class name
            controller_class_name = "Controller" + Inflector.camelcase_with_separator( controller_file_name, "_" )  
            # init class object
            controller = Kernel.const_get( controller_class_name ).new
            # store controller
            @@controllers_cache[controller_file_name] = controller
        end
        return controller
    end
  
end