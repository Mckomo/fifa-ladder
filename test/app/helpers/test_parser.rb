require './../app/classes/helpers/parser.rb'
require 'test/unit'

class TestParser < Test::Unit::TestCase

  def test_parsing_match_result
    input   = ["3 do 0", "3:0", "3 : 0", "3 0", "3 - 0"]
    input.each { |i| 
      result = Parser.match_result( i )
      assert_equal( 3, result[0] ) 
      assert_equal( 0, result[1] )
    }
  end
  
  def test_parsing_player_list_downcase 
    input = "Marek"
    result = Parser.player_list( input )
    
    assert_equal( 'marek', result[0] )
  end
  
  
  def test_parsing_player_list_sort 
    input = "marek arbuz"
    result = Parser.player_list( input )
    
    assert_equal( 'arbuz', result[0] )
    assert_equal( 'marek', result[1] )
  end
  
  def test_parsing_player_list_separators
    input = "Maciej, marek - Arbuz"
    result = Parser.player_list( input )
    
    assert_equal( 'arbuz', result[0] )
    assert_equal( 'maciej', result[1] )
    assert_equal( 'marek', result[2] )

  end

end